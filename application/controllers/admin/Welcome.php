<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_admin');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		if (!$this->session->userdata('status_login')) {
            $data = array('page_title' => "Login",);
            $this->load->view('admin/v_login', $data);
        }else{
			$data = array('page_title' => "Admin SPK Pelurusan Rambut - Beranda",);
            $this->load->view('admin/v_dashboard', $data);
		}
	}

	public function proses_login()
	{
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		$where = array(
			'username' 	=> $username,
			'password'	=> $password
		);
		$cek_login = $this->M_admin->cek_login($where)->num_rows();
		if($cek_login > 0){
			$data_login = $this->M_admin->cek_login($where)->row();
			$data_session = array(
				'kdAdmin'       => $data_login->kdAdmin,
				'nama'          => $data_login->nama,
				'status_login'  => true
			);
			$this->session->set_userdata($data_session);
			redirect(base_url('admin'));
		}else{
			$this->session->set_flashdata("alert", 
				"<script>
					toastr.error('Username dan password tidak valid !!!')
				</script>"
			);
			redirect(base_url('admin'));
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
        redirect(base_url('admin'));
	}
}
