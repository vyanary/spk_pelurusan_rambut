<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_admin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_admin');
		if (!$this->session->userdata('status_login')) {
			redirect(base_url('admin'));
		}
	}

	public function index()
	{
		$data = array('data_admin' => $this->M_admin->get_all() );
		$this->load->view('admin/admin/v_index', $data);
	}

	public function tambah()
	{
		$data = array('type' => 'tambah');
		$this->load->view('admin/admin/v_form', $data);
	}

	public function edit($kd)
	{
		$data = array(
			'type' =>'edit',
			'data_admin' => $this->M_admin->get_by_kd($kd)
		);
		$this->load->view('admin/admin/v_form', $data);
	}

	public function proses_simpan()
	{
		if($this->input->post('type')=="tambah"){
			$data = array(
				'username' 		=> $this->input->post('username'),
				'password' 		=> md5($this->input->post('password')),
				'nama' 		=> $this->input->post('nama'),
			);
			if($this->M_admin->insert($data) >= 0){
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Berhasi!",
					text: "Data Berhasil diSimpan!",
					icon: "success"
				})');
				redirect(base_url('admin/user_admin'));
			}else{
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Gagal!",
					text: "Data Gagal diSimpan!",
					icon: "error"
				})');
				redirect(base_url('admin/admin'));
			}
		}elseif($this->input->post('type')=="edit"){
			$data = array(
				'username' 		=> $this->input->post('username'),
				'password' 		=> md5($this->input->post('password')),
				'nama' 		=> $this->input->post('nama'),
			);
			if($this->M_admin->update($this->input->post('kdadmin'), $data) >= 0){
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Berhasi!",
					text: "Data Berhasil diEdit!",
					icon: "success"
				})');
				redirect(base_url('admin/user_admin'));
			}else{
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Gagal!",
					text: "Data Gagal diEdit!",
					icon: "error"
				})');
				redirect(base_url('admin/user_admin'));
			}
		}else{
			redirect(base_url('admin/admin'));
		}
	}

	public function hapus($kd)
	{
		if($this->M_admin->delete($kd) >= 0){
			$this->session->set_flashdata("alert", 'Swal.fire({
				title: "Berhasi!",
				text: "Data Berhasil diHapus!",
				icon: "success"
			})');
			redirect(base_url('admin/user_admin'));
		}else{
			$this->session->set_flashdata("alert", 'Swal.fire({
				title: "Gagal!",
				text: "Data Gagal diHapus!",
				icon: "error"
			})');
			redirect(base_url('admin/user'));
		}
	}
}
