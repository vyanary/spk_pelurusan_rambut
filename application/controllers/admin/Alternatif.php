<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alternatif extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_alternatif');
		$this->load->model('M_kriteria');
		$this->load->model('M_layanan');
		$this->load->model('M_obat');
		if (!$this->session->userdata('status_login')) {
			redirect(base_url('admin'));
		}
	}

	public function index()
	{
		$data = array('data_alternatif' => $this->M_alternatif->get_all() );
		$this->load->view('admin/alternatif/v_index', $data);
	}

	public function tambah()
	{
		$data = array(
			'type' 			=> 'tambah',
			'data_layanan'	=> $this->M_layanan->get_all(),
			'data_obat'		=> $this->M_obat->get_all(),
			'data_kriteria'	=> $this->M_kriteria->get_all()
		);
		$this->load->view('admin/alternatif/v_form', $data);
	}

	public function edit($kd)
	{
		$data = array(
			'type' 				=>'edit',
			'data_kriteria'		=> $this->M_kriteria->get_all(),
			'data_layanan'		=> $this->M_layanan->get_all(),
			'data_obat'			=> $this->M_obat->get_all(),
			'data_alternatif'	=> $this->M_alternatif->get_by_kd($kd)
		);
		$this->load->view('admin/alternatif/v_form', $data);
	}

	public function proses_simpan()
	{
		if($this->input->post('type')=="tambah"){
			$data = array(
				'kdLayanan' 	=> $this->input->post('kdLayanan'),
				'kdObat' 		=> $this->input->post('kdObat'), 
			);
			if($this->M_alternatif->insert($data) >= 0){
				$last_id = $this->db->insert_id();
				foreach ($this->M_kriteria->get_all() as $kritera) {
					$data_nilai = array(
						'kdAlternatif'	=> $last_id,
						'kdKriteria' 	=> $kritera->kdKriteria,
						'kdSubKriteria'	=> $this->input->post($kritera->kdKriteria), 
					);
					$this->M_alternatif->insert_to_nilai($data_nilai);
				}
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Berhasi!",
					text: "Data Berhasil diSimpan!",
					icon: "success"
				})');
				redirect(base_url('admin/alternatif'));
			}else{
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Gagal!",
					text: "Data Gagal diSimpan!",
					icon: "error"
				})');
				redirect(base_url('admin/alternatif'));
			}
		}elseif($this->input->post('type')=="edit"){
			$data = array(
				'kdLayanan' 	=> $this->input->post('kdLayanan'),
				'kdObat' 		=> $this->input->post('kdObat'), 
			);
			if($this->M_alternatif->update($this->input->post('kdAlternatif'), $data) >= 0){
				$this->M_alternatif->delete_nilai($this->input->post('kdAlternatif'));
				foreach ($this->M_kriteria->get_all() as $kritera) {
					$data_nilai = array(
						'kdAlternatif'	=> $this->input->post('kdAlternatif'),
						'kdKriteria' 	=> $kritera->kdKriteria,
						'kdSubKriteria'	=> $this->input->post($kritera->kdKriteria), 
					);
					$this->M_alternatif->insert_to_nilai($data_nilai);
				}
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Berhasi!",
					text: "Data Berhasil diEdit!",
					icon: "success"
				})');
				redirect(base_url('admin/alternatif'));
			}else{
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Gagal!",
					text: "Data Gagal diEdit!",
					icon: "error"
				})');
				redirect(base_url('admin/alternatif'));
			}
		}else{
			redirect(base_url('admin/alternatif'));
		}
	}

	public function hapus($kd)
	{
		$this->M_alternatif->delete_nilai($kd);
		if($this->M_alternatif->delete($kd) >= 0){
			$this->session->set_flashdata("alert", 'Swal.fire({
				title: "Berhasi!",
				text: "Data Berhasil diHapus!",
				icon: "success"
			})');
			redirect(base_url('admin/alternatif'));
		}else{
			$this->session->set_flashdata("alert", 'Swal.fire({
				title: "Gagal!",
				text: "Data Gagal diHapus!",
				icon: "error"
			})');
			redirect(base_url('admin/alternatif'));
		}
	}
}
