<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_obat');
		if (!$this->session->userdata('status_login')) {
			redirect(base_url('admin'));
		}
	}

	public function index()
	{
		$data = array('data_obat' => $this->M_obat->get_all() );
		$this->load->view('admin/obat/v_index', $data);
	}

	public function tambah()
	{
		$data = array('type' => 'tambah');
		$this->load->view('admin/obat/v_form', $data);
	}

	public function edit($kd)
	{
		$data = array(
			'type' =>'edit',
			'data_obat' => $this->M_obat->get_by_kd($kd)
		);
		$this->load->view('admin/obat/v_form', $data);
	}

	public function proses_simpan()
	{
		if($this->input->post('type')=="tambah"){
			$data = array(
				'obat' 		=> $this->input->post('obat'),
			);
			if($this->M_obat->insert($data) >= 0){
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Berhasi!",
					text: "Data Berhasil diSimpan!",
					icon: "success"
				})');
				redirect(base_url('admin/obat'));
			}else{
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Gagal!",
					text: "Data Gagal diSimpan!",
					icon: "error"
				})');
				redirect(base_url('admin/obat'));
			}
		}elseif($this->input->post('type')=="edit"){
			$data = array(
				'obat' 		=> $this->input->post('obat'),
			);
			if($this->M_obat->update($this->input->post('kdobat'), $data) >= 0){
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Berhasi!",
					text: "Data Berhasil diEdit!",
					icon: "success"
				})');
				redirect(base_url('admin/obat'));
			}else{
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Gagal!",
					text: "Data Gagal diEdit!",
					icon: "error"
				})');
				redirect(base_url('admin/obat'));
			}
		}else{
			redirect(base_url('admin/obat'));
		}
	}

	public function hapus($kd)
	{
		if($this->M_obat->delete($kd) >= 0){
			$this->session->set_flashdata("alert", 'Swal.fire({
				title: "Berhasi!",
				text: "Data Berhasil diHapus!",
				icon: "success"
			})');
			redirect(base_url('admin/obat'));
		}else{
			$this->session->set_flashdata("alert", 'Swal.fire({
				title: "Gagal!",
				text: "Data Gagal diHapus!",
				icon: "error"
			})');
			redirect(base_url('admin/obat'));
		}
	}
}
