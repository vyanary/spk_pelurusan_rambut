<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_layanan');
		if (!$this->session->userdata('status_login')) {
			redirect(base_url('admin'));
		}
	}

	public function index()
	{
		$data = array('data_layanan' => $this->M_layanan->get_all() );
		$this->load->view('admin/layanan/v_index', $data);
	}

	public function tambah()
	{
		$data = array('type' => 'tambah');
		$this->load->view('admin/layanan/v_form', $data);
	}

	public function edit($kd)
	{
		$data = array(
			'type' =>'edit',
			'data_layanan' => $this->M_layanan->get_by_kd($kd)
		);
		$this->load->view('admin/layanan/v_form', $data);
	}

	public function proses_simpan()
	{
		if($this->input->post('type')=="tambah"){
			$data = array(
				'layanan' 		=> $this->input->post('layanan'),
			);
			if($this->M_layanan->insert($data) >= 0){
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Berhasi!",
					text: "Data Berhasil diSimpan!",
					icon: "success"
				})');
				redirect(base_url('admin/layanan'));
			}else{
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Gagal!",
					text: "Data Gagal diSimpan!",
					icon: "error"
				})');
				redirect(base_url('admin/layanan'));
			}
		}elseif($this->input->post('type')=="edit"){
			$data = array(
				'layanan' 		=> $this->input->post('layanan'),
			);
			if($this->M_layanan->update($this->input->post('kdLayanan'), $data) >= 0){
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Berhasi!",
					text: "Data Berhasil diEdit!",
					icon: "success"
				})');
				redirect(base_url('admin/layanan'));
			}else{
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Gagal!",
					text: "Data Gagal diEdit!",
					icon: "error"
				})');
				redirect(base_url('admin/layanan'));
			}
		}else{
			redirect(base_url('admin/layanan'));
		}
	}

	public function hapus($kd)
	{
		if($this->M_layanan->delete($kd) >= 0){
			$this->session->set_flashdata("alert", 'Swal.fire({
				title: "Berhasi!",
				text: "Data Berhasil diHapus!",
				icon: "success"
			})');
			redirect(base_url('admin/layanan'));
		}else{
			$this->session->set_flashdata("alert", 'Swal.fire({
				title: "Gagal!",
				text: "Data Gagal diHapus!",
				icon: "error"
			})');
			redirect(base_url('admin/layanan'));
		}
	}
}
