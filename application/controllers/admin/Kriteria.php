<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kriteria extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_kriteria');
		if (!$this->session->userdata('status_login')) {
			redirect(base_url('admin'));
		}
	}

	public function index()
	{
		$data = array('data_kriteria' => $this->M_kriteria->get_all() );
		$this->load->view('admin/kriteria/v_index', $data);
	}

	public function tambah()
	{
		$data = array('type' => 'tambah');
		$this->load->view('admin/kriteria/v_form', $data);
	}

	public function edit($kd)
	{
		$data = array(
			'type' =>'edit',
			'data_kriteria' => $this->M_kriteria->get_by_kd($kd)
		);
		$this->load->view('admin/kriteria/v_form', $data);
	}

	public function proses_simpan()
	{
		if($this->input->post('type')=="tambah"){
			$data = array(
				'kriteria' 		=> $this->input->post('kriteria'),
				'sifat' 		=> $this->input->post('sifat'), 
				'pertanyaan' 	=> $this->input->post('pertanyaan'),
			);
			$insert_kriteria = $this->M_kriteria->insert($data);
			$last_id=$this->db->insert_id();
			if($insert_kriteria >= 0){
				for ($i=0; $i < 4; $i++) { 
					$data_subkriteria = array(
						'kdKriteria' 	=> $last_id,
						'subKriteria'	=> $this->input->post('subKriteria')[$i], 
						'value' 		=> $this->input->post('value')[$i],
					);
					$this->M_kriteria->insert_to_subkriteria($data_subkriteria);
				}
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Berhasi!",
					text: "Data Berhasil diSimpan!",
					icon: "success"
				})');
				redirect(base_url('admin/kriteria'));
			}else{
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Gagal!",
					text: "Data Gagal diSimpan!",
					icon: "error"
				})');
				redirect(base_url('admin/kriteria'));
			}
		}elseif($this->input->post('type')=="edit"){
			$data = array(
				'kriteria' 		=> $this->input->post('kriteria'),
				'sifat' 		=> $this->input->post('sifat'), 
				'pertanyaan' 	=> $this->input->post('pertanyaan'),
			);
			$update_kriteria = $this->M_kriteria->update($this->input->post('kdKriteria'), $data);
			if($update_kriteria >= 0){
				for ($i=0; $i < 4; $i++) { 
					$data_subkriteria = array(
						'kdKriteria' 	=> $this->input->post('kdKriteria'),
						'subKriteria'	=> $this->input->post('subKriteria')[$i], 
						'value' 		=> $this->input->post('value')[$i],
					);
					$this->M_kriteria->update_to_subkriteria($this->input->post('kdSubKriteria')[$i], $data_subkriteria);
				}

				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Berhasi!",
					text: "Data Berhasil diEdit!",
					icon: "success"
				})');
				redirect(base_url('admin/kriteria'));
			}else{
				$this->session->set_flashdata("alert", 'Swal.fire({
					title: "Gagal!",
					text: "Data Gagal diEdit!",
					icon: "error"
				})');
				redirect(base_url('admin/kriteria'));
			}
		}else{
			redirect(base_url('admin/kriteria'));
		}
	}

	public function hapus($kd)
	{
		$this->M_kriteria->delete_subkriteria($kd);
		if($this->M_kriteria->delete($kd) >= 0){
			$this->session->set_flashdata("alert", 'Swal.fire({
				title: "Berhasi!",
				text: "Data Berhasil diHapus!",
				icon: "success"
			})');
			redirect(base_url('admin/kriteria'));
		}else{
			$this->session->set_flashdata("alert", 'Swal.fire({
				title: "Gagal!",
				text: "Data Gagal diHapus!",
				icon: "error"
			})');
			redirect(base_url('admin/kriteria'));
		}
	}
}
