	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/AdminLTE-3.0.5/dist/css/adminlte.min.css">
  	<!-- Google Font: Source Sans Pro -->
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition layout-top-nav">
	<div class="wrapper">
		<!-- Navbar -->
		<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<a href="<?php echo base_url() ?>" class="navbar-brand">
							<img src="<?php echo base_url() ?>assets/AdminLTE-3.0.5/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
								style="opacity: .8">
							<span class="brand-text font-weight-light">SPK Pelurusan Rambut Smothing dan Rebounding</span>
						</a>
					</div>
				</div>
			</div>
		</nav>
		<!-- /.navbar -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<div class="content-header">
				<div class="container">
					<div class="row mb-2">
						<div class="col-sm-12 text-center">
							<h1 class="m-0 text-dark"><?php echo ""; ?></h1>
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->