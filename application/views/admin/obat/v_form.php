<?php $this->load->view('layout/head') ?>

<?php $this->load->view('layout/navbar') ?>

<?php $this->load->view('layout/sidebar') ?>

<!-- Main content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card card-primary card-outline">
					<form role="form" method="post" action="<?php echo base_url('admin/obat/proses_simpan') ?>">
						<div class="card-body">
							<?php if($type=="edit"){ ?>
								<input type="hidden" value="<?php echo $data_obat->kdObat ?>" name="kdObat">
							<?php } ?>
							<input type="hidden" value="<?php echo $type ?>" name="type">	
							<div class="form-group">
								<label >Obat</label>
								<input type="text" value="<?php echo ($type=="edit") ? $data_obat->obat : "" ; ?>" class="form-control" required name="obat" placeholder="Masukkan obat">
							</div>
						</div>
						<!-- /.card-body -->

						<div class="card-footer">
							<button type="submit" class="btn btn-primary"><?php echo ($type=="tambah") ? "Simpan" : "Edit" ; ?></button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<?php $this->load->view('layout/footer') ?>
<?php $this->load->view('layout/end') ?>
