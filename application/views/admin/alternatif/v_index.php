<?php $this->load->view('layout/head') ?>

<?php $this->load->view('layout/navbar') ?>

<?php $this->load->view('layout/sidebar') ?>

<!-- Main content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<a href="<?php echo base_url('admin/'.$this->uri->segment(2).'/tambah') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
					</div>
					<div class="card-body p-0">
						<table class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px">No.</th>
									<th>Layanan</th>
									<th>Obat</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=0; foreach ($data_alternatif as $alternatif) { ?>
									<tr>
										<td><?php echo ++$no; ?>.</td>
										<td><?php echo $alternatif->layanan; ?></td>
										<td><?php echo $alternatif->obat; ?></td>
										<td>
											<a href="<?php echo base_url('admin/'.$this->uri->segment(2).'/edit/'.$alternatif->kdAlternatif); ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
											<a href="<?php echo base_url('admin/'.$this->uri->segment(2).'/hapus/'.$alternatif->kdAlternatif); ?>" onclick="return confirm('Anda yakin ingin menghapus data ini ?')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</a>
											<button data-toggle="modal" data-target="#modal-alternatif-<?php echo $alternatif->kdAlternatif ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i> Detail</button>
										</td>
									</tr>
								<?php } ?>	
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<?php foreach ($data_alternatif as $alternatif) { ?>
			<div class="modal fade" id="modal-alternatif-<?php echo $alternatif->kdAlternatif ?>">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Detail Alternatif</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" style="overflow-x: auto;">
							<table cellpadding="5" style="border-collapse: collapse;" border="0">
								<tr>
									<td>Layanan</td>
									<td>:</td>
									<td><?php echo $alternatif->layanan; ?></td>
								</tr>
								<tr>
									<td>Obat</td>
									<td>:</td>
									<td><?php echo $alternatif->obat; ?></td>
								</tr>
							</table>
							<table class="table table-striped">
								<tr>
									<th>Kriteria</th>
									<th>Sub Kriteria</th>
									<th>Bobot</th>
								</tr>
								<?php foreach ($this->M_alternatif->get_nilai($alternatif->kdAlternatif) as $nilai) { ?>
									<tr>
										<td><?php echo $nilai->kriteria ?></td>
										<td><?php echo $nilai->subKriteria ?></td>
										<td><?php echo $nilai->value ?></td>
									</tr>
								<?php } ?>
							</table>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
		<?php } ?>
	</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<?php $this->load->view('layout/footer') ?>
<?php $this->load->view('layout/end') ?>
