<?php $this->load->view('layout/head') ?>

<?php $this->load->view('layout/navbar') ?>

<?php $this->load->view('layout/sidebar') ?>

<!-- Main content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card card-primary card-outline">
					<form role="form" method="post" action="<?php echo base_url('admin/alternatif/proses_simpan') ?>">
						<div class="card-body">
							<?php if($type=="edit"){ ?>
								<input type="hidden" value="<?php echo $data_alternatif->kdAlternatif ?>" name="kdAlternatif">
							<?php } ?>
							<input type="hidden" value="<?php echo $type ?>" name="type">	
							<div class="form-group">
								<label >Layanan</label>
								<select name="kdLayanan" class="form-control" required>
									<option value="">-- Pilih Layanan --</option>
									<?php foreach ($data_layanan as $layanan) { ?>
										<option value="<?php echo $layanan->kdLayanan ?>" <?php if($type=="edit"){ echo ($data_alternatif->kdLayanan==$layanan->kdLayanan) ? "SELECTED" : ""; } ?>><?php echo $layanan->layanan ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label >Obat</label>
								<select name="kdObat" class="form-control" required>
									<option value="">-- Pilih Layanan --</option>
									<?php foreach ($data_obat as $obat) { ?>
										<option value="<?php echo $obat->kdObat ?>" <?php if($type=="edit"){ echo ($data_alternatif->kdObat==$obat->kdObat) ? "SELECTED" : ""; } ?>><?php echo $obat->obat ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<table class="table table-striped">
									<thead>
										<th>Kriteria</th>
										<th colspan="4">Nilai</th>
									</thead>
									<tbody>
										<?php foreach ($data_kriteria as $kriteria) { ?>
											<tr>
												<td><?php echo $kriteria->kriteria ?></td>
												<?php foreach ($this->M_kriteria->get_sub_kriteria($kriteria->kdKriteria) as $subkriteria) { ?>
													<td>
														<div class="form-check">
															<input class="form-check-input" value="<?php echo $subkriteria->kdSubKriteria ?>" type="radio" name="<?php echo $subkriteria->kdKriteria ?>" 
																<?php 
																	if($type=="edit"){ 
																		if($this->M_alternatif->get_nilai_by_alternatif_and_kriteria($data_alternatif->kdAlternatif,$kriteria->kdKriteria)->kdSubKriteria==$subkriteria->kdSubKriteria){ 
																			echo "checked"; 
																		} 
																	} 
																?>
															>
															<label class="form-check-label"><?php echo $subkriteria->subKriteria ?></label>
														</div>
													</td>
												<?php } ?>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- /.card-body -->

						<div class="card-footer">
							<button type="submit" class="btn btn-primary"><?php echo ($type=="tambah") ? "Simpan" : "Edit" ; ?></button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<?php $this->load->view('layout/footer') ?>
<?php $this->load->view('layout/end') ?>
