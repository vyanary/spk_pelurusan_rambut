<?php $this->load->view('layout/head') ?>

<?php $this->load->view('layout/navbar') ?>

<?php $this->load->view('layout/sidebar') ?>

<!-- Main content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card card-primary card-outline">
					<form role="form" method="post" action="<?php echo base_url('admin/layanan/proses_simpan') ?>">
						<div class="card-body">
							<?php if($type=="edit"){ ?>
								<input type="hidden" value="<?php echo $data_layanan->kdLayanan ?>" name="kdLayanan">
							<?php } ?>
							<input type="hidden" value="<?php echo $type ?>" name="type">	
							<div class="form-group">
								<label >Layanan</label>
								<input type="text" value="<?php echo ($type=="edit") ? $data_layanan->layanan : "" ; ?>" class="form-control" required name="layanan" placeholder="Masukkan layanan">
							</div>
						</div>
						<!-- /.card-body -->

						<div class="card-footer">
							<button type="submit" class="btn btn-primary"><?php echo ($type=="tambah") ? "Simpan" : "Edit" ; ?></button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<?php $this->load->view('layout/footer') ?>
<?php $this->load->view('layout/end') ?>
