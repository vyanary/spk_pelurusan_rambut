<?php $this->load->view('layout/head') ?>	
  		<!-- icheck bootstrap -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/AdminLTE-3.0.5/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/AdminLTE-3.0.5/dist/css/adminlte.min.css">
  		<!-- Google Font: Source Sans Pro -->
  		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">  
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href="<?php echo base_url() ?>assets/AdminLTE-3.0.5/index2.html"><b>Admin</b>LTE</a>
			</div>
			<!-- /.login-logo -->
			<div class="card">
				<div class="card-body login-card-body">
					<p class="login-box-msg">Sign in to start your session</p>

					<form action="<?php echo base_url('admin/welcome/proses_login') ?>" method="post">
						<div class="input-group mb-3">
							<input type="text" name="username" class="form-control" placeholder="Username">
							<div class="input-group-append">
								<div class="input-group-text">
									<span class="fas fa-user"></span>
								</div>
							</div>
						</div>
						<div class="input-group mb-3">
							<input type="password" name="password" class="form-control" placeholder="Password">
							<div class="input-group-append">
								<div class="input-group-text">
									<span class="fas fa-lock"></span>
								</div>
							</div>
						</div>
						<div class="row">
							<!-- /.col -->
							<div class="col-12">
								<button type="submit" class="btn btn-primary btn-block">Sign In</button>
							</div>
							<!-- /.col -->
						</div>
					</form>
				</div>
				<!-- /.login-card-body -->
			</div>
		</div>
		<!-- /.login-box -->
		<!-- REQUIRED SCRIPTS -->

		<!-- jQuery -->
		<script src="<?php echo base_url() ?>assets/AdminLTE-3.0.5/plugins/jquery/jquery.min.js"></script>
		<!-- Bootstrap 4 -->
		<script src="<?php echo base_url() ?>assets/AdminLTE-3.0.5/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
		<!-- SweetAlert2 -->
		<script src="<?php echo base_url() ?>assets/AdminLTE-3.0.5/plugins/sweetalert2/sweetalert2.min.js"></script>
		<!-- Toastr -->
		<script src="<?php echo base_url() ?>assets/AdminLTE-3.0.5/plugins/toastr/toastr.min.js"></script>
		<!-- AdminLTE App -->
		<script src="<?php echo base_url() ?>assets/AdminLTE-3.0.5/dist/js/adminlte.min.js"></script>

		<?php echo $this->session->flashdata('alert'); ?>
		
	</body>
</html>
