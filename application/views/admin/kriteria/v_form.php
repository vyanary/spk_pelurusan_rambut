<?php $this->load->view('layout/head') ?>

<?php $this->load->view('layout/navbar') ?>

<?php $this->load->view('layout/sidebar') ?>

<!-- Main content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card card-primary card-outline">
					<form role="form" method="post" action="<?php echo base_url('admin/kriteria/proses_simpan') ?>">
						<div class="card-body">
							<?php if($type=="edit"){ ?>
								<input type="hidden" value="<?php echo $data_kriteria->kdKriteria ?>" name="kdKriteria">
							<?php } ?>
							<input type="hidden" value="<?php echo $type ?>" name="type">	
							<div class="form-group">
								<label >Kriteria</label>
								<input type="text" value="<?php echo ($type=="edit") ? $data_kriteria->kriteria : "" ; ?>" class="form-control" required name="kriteria" placeholder="Masukkan Kriteria">
							</div>
							<div class="form-group">
								<label >Sifat</label>
								<div class="row no-gutters">
									<div class="col-1">
										<div class="form-check">
											<input class="form-check-input" value="B" type="radio" name="sifat" <?php if($type=="edit"){ if($data_kriteria->sifat=="B"){ echo "checked"; } }else{ echo "checked"; } ?>>
											<label class="form-check-label">Benefit</label>
										</div>
									</div>
									<div class="col-1">
										<div class="form-check">
											<input class="form-check-input" value="C" type="radio" name="sifat" <?php if($type=="edit"){ if($data_kriteria->sifat=="C"){ echo "checked"; } } ?>>
											<label class="form-check-label">Cost</label>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label >Pertanyaan</label>
								<input type="text" value="<?php echo ($type=="edit") ? $data_kriteria->pertanyaan : "" ; ?>" class="form-control" required name="pertanyaan" placeholder="Masukkan Pertanyaan">
							</div>
							<div class="form-group">
								<label >Sub Kriteria</label>
								<?php for($i=0;$i<4;$i++){ ?>
									<div class="row mb-3">
										<div class="col-12">
											<input type="text" value="<?php echo ($type=="edit") ? $this->M_kriteria->get_sub_kriteria($data_kriteria->kdKriteria)[$i]->subKriteria : "" ; ?>" class="form-control" required name="subKriteria[]" placeholder="Masukkan Sub Kriteria">
											<input type="hidden" value="<?php echo 4-$i ?>" class="form-control" required name="value[]">
											<input type="hidden" value="<?php echo ($type=="edit") ? $this->M_kriteria->get_sub_kriteria($data_kriteria->kdKriteria)[$i]->kdSubKriteria : "" ; ?>" class="form-control" required name="kdSubKriteria[]">
										</div>
										<div class="col-6">
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
						<!-- /.card-body -->

						<div class="card-footer">
							<button type="submit" class="btn btn-primary"><?php echo ($type=="tambah") ? "Simpan" : "Edit" ; ?></button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<?php $this->load->view('layout/footer') ?>
<?php $this->load->view('layout/end') ?>
