<?php $this->load->view('layout/head') ?>

<?php $this->load->view('layout/navbar') ?>

<?php $this->load->view('layout/sidebar') ?>

<!-- Main content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<a href="<?php echo base_url('admin/'.$this->uri->segment(2).'/tambah') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
					</div>
					<div class="card-body p-0">
						<table class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px">No.</th>
									<th>Kriteria</th>
									<th>Sifat</th>
									<th>Pertanyaan</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=0; foreach ($data_kriteria as $kriteria) { ?>
									<tr>
										<td><?php echo ++$no; ?>.</td>
										<td><?php echo $kriteria->kriteria; ?></td>
										<td><?php echo ($kriteria->sifat=="B") ? "Benefit" : "Cost" ; ?></td>
										<td><?php echo $kriteria->pertanyaan; ?></td>
										<td>
											<a href="<?php echo base_url('admin/'.$this->uri->segment(2).'/edit/'.$kriteria->kdKriteria); ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
											<a href="<?php echo base_url('admin/'.$this->uri->segment(2).'/hapus/'.$kriteria->kdKriteria); ?>" onclick="return confirm('Anda yakin ingin menghapus data ini ?')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</a>
											<button data-toggle="modal" data-target="#modal-kriteria-<?php echo $kriteria->kdKriteria ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i> Detail</button>
										</td>
									</tr>
								<?php } ?>	
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<?php foreach ($data_kriteria as $kriteria) { ?>
			<div class="modal fade" id="modal-kriteria-<?php echo $kriteria->kdKriteria ?>">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Detail Kriteria</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" style="overflow-x: auto;">
							<table cellpadding="5" style="border-collapse: collapse;" border="0">
								<tr>
									<td>Kriteria</td>
									<td>:</td>
									<td><?php echo $kriteria->kriteria; ?></td>
								</tr>
								<tr>
									<td>Pertanyaan</td>
									<td>:</td>
									<td><?php echo $kriteria->pertanyaan; ?></td>
								</tr>
							</table>
							<table class="table table-striped">
								<tr>
									<th>Sub Kriteria</th>
									<th>Value</th>
								</tr>
								<?php foreach ($this->M_kriteria->get_sub_kriteria($kriteria->kdKriteria) as $nilai) { ?>
									<tr>
										<td><?php echo $nilai->subKriteria ?></td>
										<td><?php echo $nilai->value ?></td>
									</tr>
								<?php } ?>
							</table>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
		<?php } ?>
	</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<?php $this->load->view('layout/footer') ?>
<?php $this->load->view('layout/end') ?>
