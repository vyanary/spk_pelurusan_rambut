<?php $this->load->view('layout/head') ?>

<?php $this->load->view('layout/navbar') ?>

<?php $this->load->view('layout/sidebar') ?>

<!-- Main content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<a href="<?php echo base_url('admin/'.$this->uri->segment(2).'/tambah') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
					</div>
					<div class="card-body p-0">
						<table class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px">No.</th>
									<th>Nama</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=0; foreach ($data_admin as $admin) { ?>
									<tr>
										<td><?php echo ++$no; ?>.</td>
										<td><?php echo $admin->nama; ?></td>
										<td>
											<a href="<?php echo base_url('admin/'.$this->uri->segment(2).'/edit/'.$admin->kdAdmin); ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
											<a href="<?php echo base_url('admin/'.$this->uri->segment(2).'/hapus/'.$admin->kdAdmin); ?>" onclick="return confirm('Anda yakin ingin menghapus data ini ?')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</a>
										</td>
									</tr>
								<?php } ?>	
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<?php $this->load->view('layout/footer') ?>
<?php $this->load->view('layout/end') ?>
