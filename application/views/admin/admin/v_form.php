<?php $this->load->view('layout/head') ?>

<?php $this->load->view('layout/navbar') ?>

<?php $this->load->view('layout/sidebar') ?>

<!-- Main content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card card-primary card-outline">
					<form role="form" method="post" action="<?php echo base_url('admin/user_admin/proses_simpan') ?>">
						<div class="card-body">
							<?php if($type=="edit"){ ?>
								<input type="hidden" value="<?php echo $data_admin->kdAdmin ?>" name="kdAdmin">
							<?php } ?>
							<input type="hidden" value="<?php echo $type ?>" name="type">	
							<div class="form-group">
								<label >Username</label>
								<input type="text" value="<?php echo ($type=="edit") ? $data_admin->username : "" ; ?>" class="form-control" required name="username" placeholder="Masukkan Username">
							</div>
							<div class="form-group">
								<label >Password</label>
								<input type="password" value="<?php echo ($type=="edit") ? $data_admin->password : "" ; ?>" class="form-control" required name="password" placeholder="Masukkan Password">
							</div>
							<div class="form-group">
								<lasbel >Nama</label>
								<input type="text" value="<?php echo ($type=="edit") ? $data_admin->nama : "" ; ?>" class="form-control" required name="nama" placeholder="Masukkan Nama">
							</div>
						</div>
						<!-- /.card-body -->

						<div class="card-footer">
							<button type="submit" class="btn btn-primary"><?php echo ($type=="tambah") ? "Simpan" : "Edit" ; ?></button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<?php $this->load->view('layout/footer') ?>
<?php $this->load->view('layout/end') ?>
