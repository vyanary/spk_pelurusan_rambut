<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<!-- Brand Logo -->
	<a href="" class="brand-link">
		<img src="<?php echo base_url() ?>assets/AdminLTE-3.0.5/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
			style="opacity: .8">
		<span class="brand-text font-weight-light">SPK Pelurusan Rambut</span>
	</a>

	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar user panel (optional) -->
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image">
				<img src="<?php echo base_url() ?>assets/AdminLTE-3.0.5/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
			</div>
			<div class="info">
				<a href="#" class="d-block"><?php echo $this->session->userdata('nama') ?></a>
			</div>
		</div>

		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<!-- Add icons to the links using the .nav-icon class
					with font-awesome or any other icon font library -->
				<li class="nav-item">
					<a href="<?php echo base_url('admin') ?>" class="nav-link <?php if($this->uri->segment(2)=="") { echo "active"; } ?>">
						<i class="nav-icon fas fa-th"></i>
						<p>Beranda</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo base_url('admin/kriteria') ?>" class="nav-link <?php if($this->uri->segment(2)=="kriteria") { echo "active"; } ?>">
						<i class="nav-icon fas fa-th"></i>
						<p>Data Kriteria</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo base_url('admin/obat') ?>" class="nav-link <?php if($this->uri->segment(2)=="obat") { echo "active"; } ?>">
						<i class="nav-icon fas fa-th"></i>
						<p>Data Obat</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo base_url('admin/layanan') ?>" class="nav-link <?php if($this->uri->segment(2)=="layanan") { echo "active"; } ?>">
						<i class="nav-icon fas fa-th"></i>
						<p>Data Layanan</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo base_url('admin/alternatif') ?>" class="nav-link <?php if($this->uri->segment(2)=="alternatif") { echo "active"; } ?>">
						<i class="nav-icon fas fa-th"></i>
						<p>Data Alternatif</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo base_url('admin/user_admin') ?>" class="nav-link <?php if($this->uri->segment(2)=="user_admin") { echo "active"; } ?>">
						<i class="nav-icon fas fa-users"></i>
						<p>Data User Admin</p>
					</a>
				</li>
			</ul>
		</nav>
		<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<?php if($this->uri->segment(2)==""){ ?>
						<h1 class="m-0 text-dark">Beranda</h1>
					<?php }else{ ?>
						<?php if($this->uri->segment(3)==""){ ?>
							<h1 class="m-0 text-dark"><?php echo "Data ".str_replace("_"," ",ucwords($this->uri->segment(2))); ?></h1>
						<?php }else{ ?>
							<h1 class="m-0 text-dark"><?php echo str_replace("_"," ",ucwords($this->uri->segment(3)))." ".str_replace("_"," ",ucwords($this->uri->segment(2))); ?></h1>
						<?php } ?>
						
					<?php } ?>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<?php if($this->uri->segment(2)!=""){ ?>
							<li class="breadcrumb-item"><a href="<?php echo base_url('admin') ?>">Beranda</a></li>
							<?php if($this->uri->segment(3)==""){ ?>
								<li class="breadcrumb-item active"><?php echo "Data ".str_replace("_"," ",ucwords($this->uri->segment(2))); ?></li>
							<?php }else{ ?>
								<li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->uri->segment(2)) ?>"><?php echo "Data ".str_replace("_"," ",ucwords($this->uri->segment(2))); ?></a></li>
								<li class="breadcrumb-item active"><?php echo str_replace("_"," ",ucwords($this->uri->segment(3))); ?></li>
							<?php } ?>
						<?php } ?>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->
