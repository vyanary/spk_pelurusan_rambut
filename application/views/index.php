<?php $this->load->view('layout/head') ?>

<?php $this->load->view('layout_user/navbar') ?>

<!-- Main content -->
<div class="content">
	<div class="container">
		<div class="row justify-content-center pb-5">
			<form action="<?php echo base_url('hasil') ?>" method="post">
				<h2>Jawab pertanyaan berikut ini !!</h2>
				<div class="col-12 my-5">
					<table cellpadding="5" style="border-collapse:collapse" border="0">
						<?php $no=0; foreach ($data_kriteria as $kriteria) { ?>
							<tr>
								<th><h3><?php echo ++$no.". ".$kriteria->pertanyaan  ?></h3></th>
							</tr>
							<?php foreach ($this->M_kriteria->get_sub_kriteria($kriteria->kdKriteria) as $subKriteria) { ?>
								<tr>
									<td>
										<div class="form-check">
											<input class="form-check-input" type="radio" value="<?php echo $subKriteria->kdSubKriteria ?>" name="<?php echo $subKriteria->kdKriteria ?>" required>
											<label class="form-check-label"><?php echo $subKriteria->subKriteria ?></label>
										</div>
									</td>
								</tr>
							<?php } ?>
						<?php } ?>
					</table>
				</div>
				<div class="col-12 text-center">
					<button type="submit" class="btn btn-primary text-center">Proses</button>
				</div>
			</form>
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<?php $this->load->view('layout/end') ?>
</body>
</html>
