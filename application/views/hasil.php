<?php $this->load->view('layout/head') ?>

<?php $this->load->view('layout_user/navbar') ?>
<?php 
	$jmlBobotYangDipilih=0;
	foreach ($data_kriteria as $kriteria1) { 
		$jmlBobotYangDipilih += $this->M_kriteria->get_sub_kriteria_by_kd($this->input->post($kriteria1->kdKriteria))->value;
	}
?>
<!-- Main content -->
<div class="content">
	<div class="container">
		<div class="row justify-content-center pb-5">
			<h2 class="mb-3">Hasil Perhitungan</h2>
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-header">
						<h3 class="card-title">Tabel Perhitungan</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<h4>1. Tabel Data Bobot yang dipilih</h4>
						<table class="table table-bordered">
							<thead>
								<th>No.</th>
								<th>Pertanyaan</th>
								<th>Jawaban</th>
								<th>Bobot</th>
							</thead>
							<tbody>
								<?php $no=0; foreach ($data_kriteria as $kriteria) { ?>
									<tr>
										
										<td><?php echo ++$no."."; ?></td>
										<td><?php echo $kriteria->pertanyaan ?></td>
										<td><?php echo $this->M_kriteria->get_sub_kriteria_by_kd($this->input->post($kriteria->kdKriteria))->subKriteria ?></td>
										<td><?php echo $this->M_kriteria->get_sub_kriteria_by_kd($this->input->post($kriteria->kdKriteria))->value/$jmlBobotYangDipilih; ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>

						<h4 class="mt-5">2. Tabel Data Alternatif</h4>
						<table class="table table-bordered">
							<thead>
								<th>No.</th>
								<th>Layanan</th>
								<th>Obat</th>
								<?php foreach ($data_kriteria as $kriteria) { ?>
									<th><?php echo $kriteria->kriteria ?></th>
								<?php } ?>
							</thead>
							<tbody>
								<?php $no=0; foreach ($data_alternatif as $alternatif) { ?>
									<tr>
										<td><?php echo ++$no."."; ?></td>
										<td><?php echo $alternatif->layanan ?></td>
										<td><?php echo $alternatif->obat ?></td>
										<?php foreach ($data_kriteria as $kriteria) { ?>
											<td><?php echo $this->M_alternatif->get_nilai_value_by_kriteria_and_alternatif($alternatif->kdAlternatif,$kriteria->kdKriteria) ?></td>
										<?php } ?>
									</tr>
								<?php } ?>
							</tbody>
						</table>

						<h4 class="mt-5">3. Tabel Normalisasi Data Alternatif sesuai sifat cost dan benefit</h4>
						<table class="table table-bordered">
							<thead>
								<th>No.</th>
								<th>Layanan</th>
								<th>Obat</th>
								<?php foreach ($data_kriteria as $kriteria) { ?>
									<th><?php echo $kriteria->kriteria ?></th>
								<?php } ?>
							</thead>
							<tbody>
								<?php $no=0; foreach ($data_alternatif as $alternatif) { ?>
									<tr>
										<td><?php echo ++$no."."; ?></td>
										<td><?php echo $alternatif->layanan ?></td>
										<td><?php echo $alternatif->obat ?></td>
										<?php foreach ($data_kriteria as $kriteria) { ?>
											<td>
												<?php 
													$nilaiMinMaxKriteria = $this->M_kriteria->get_min_max_nilai_alternatif($kriteria->kdKriteria);
													$nilaiAlternatif = $this->M_alternatif->get_nilai_value_by_kriteria_and_alternatif($alternatif->kdAlternatif,$kriteria->kdKriteria);
													if($kriteria->sifat=="B"){
														echo $nilaiAlternatif/$nilaiMinMaxKriteria;
													}else{
														echo $nilaiMinMaxKriteria/$nilaiAlternatif;
													}
												?>
											</td>
										<?php } ?>
									</tr>
								<?php } ?>
							</tbody>
						</table>

						<h5 class="mt-2">Keterangan : </h5>
						<table class="table table-bordered">
							<thead>
								<th>No.</th>
								<th>Kriteria</th>
								<th>Sifat</th>
								<th>Nilai Min/Max</th>
							</thead>
							<tbody>
								<?php $no=0; foreach ($data_kriteria as $kriteria) { ?>
									<tr>
										<td><?php echo ++$no."."; ?></td>
										<td><?php echo $kriteria->kriteria ?></td>
										<td><?php echo ($kriteria->sifat=="B") ? "Benefit" : "Cost"; ?></td>
										<td>
											<?php echo $this->M_kriteria->get_min_max_nilai_alternatif($kriteria->kdKriteria)." - "; ?>
											<?php echo ($kriteria->sifat=="B") ? "Maximum" : "Minimum"; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>

						<h4 class="mt-5">4. Tabel Data Normalisasi dikali bobot yang dipilih</h4>
						<table class="table table-bordered">
							<thead>
								<th>No.</th>
								<th>Layanan</th>
								<th>Obat</th>
								<?php foreach ($data_kriteria as $kriteria) { ?>
									<th><?php echo $kriteria->kriteria ?></th>
								<?php } ?>
							</thead>
							<tbody>
								<?php $array_rangking=[]; $no=0; foreach ($data_alternatif as $alternatif) { ?>
									<tr>
										<td><?php echo ++$no."."; ?></td>
										<td><?php echo $alternatif->layanan ?></td>
										<td><?php echo $alternatif->obat ?></td>
										<?php $total=0; foreach ($data_kriteria as $kriteria) { ?>
											<td>
												<?php 
													$bobotDipilih = $this->M_kriteria->get_sub_kriteria_by_kd($this->input->post($kriteria->kdKriteria))->value/$jmlBobotYangDipilih;
													$nilaiMinMaxKriteria = $this->M_kriteria->get_min_max_nilai_alternatif($kriteria->kdKriteria);
													$nilaiAlternatif = $this->M_alternatif->get_nilai_value_by_kriteria_and_alternatif($alternatif->kdAlternatif,$kriteria->kdKriteria);
													if($kriteria->sifat=="B"){
														echo $bobotDipilih*($nilaiAlternatif/$nilaiMinMaxKriteria);
														$total+=$bobotDipilih*($nilaiAlternatif/$nilaiMinMaxKriteria);
													}else{
														echo $bobotDipilih*($nilaiMinMaxKriteria/$nilaiAlternatif);
														$total+=$bobotDipilih*($nilaiMinMaxKriteria/$nilaiAlternatif);
													}
												?>
											</td>
										<?php } ?>
									</tr>
									<?php 
										$array_rangking[$alternatif->kdAlternatif] = $total;
									?>
								<?php } ?>
							</tbody>
						</table>

						<h4 class="mt-5">5. Tabel Perangkingan</h4>
						<table class="table table-bordered">
							<thead>
								<th>Rangking</th>
								<th>Layanan</th>
								<th>Obat</th>
								<th>Total</th>
							</thead>
							<tbody>
								<?php arsort($array_rangking); $no=0; foreach ($array_rangking as $kdAlternatif=>$total) { ?>
									<?php $alternatif = $this->M_alternatif->get_by_kd($kdAlternatif); ?>
									<tr>
										<td><?php echo ++$no."."; ?></td>
										<td><?php echo $alternatif->layanan ?></td>
										<td><?php echo $alternatif->obat ?></td>
										<td><?php echo $total; ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>

						<h2 class="mt-5 text-center">KESIMPULAN</h2>
						<div class="alert alert-info" role="alert">
							<?php $kesimpulan = $this->M_alternatif->get_by_kd(array_keys($array_rangking)[0]); ?>
                            <h4> 
								Hasil perhitungan yang dilakukan menggunakan metode SAW maka layanan dan obat peluruasn rambut terbaik untuk di pilih adalah
								Layanan <b><?php echo $kesimpulan->layanan ?></b> dan Obat <b><?php echo $kesimpulan->obat ?></b> dengan nilai <b><?php echo $array_rangking[$kesimpulan->kdAlternatif]; ?></b>
							</h4>
                        </div>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<?php $this->load->view('layout/end') ?>
</body>
</html>
