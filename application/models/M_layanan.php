<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_layanan extends CI_Model
{

    public $table = 'layanan';
    public $kd = 'kdLayanan';
    public $order = 'DESC';

    // get all
    function get_all()
    {
        $this->db->order_by($this->kd, $this->order);
        return $this->db->get($this->table)->result();
	}
	
    function get_where($where)
    {
        $this->db->where($where);
        $this->db->order_by($this->kd, $this->order);
        return $this->db->get($this->table);
    }

    // get data by kd
    function get_by_kd($kd)
    {
        $this->db->where($this->kd, $kd);
        return $this->db->get($this->table)->row();
    }

    // insert data
    function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    // update data
    function update($kd, $data)
    {
        $this->db->where('kdLayanan', $kd);
        return $this->db->update($this->table, $data);
	}
	
	public function delete($kd)
	{
		$this->db->where('kdLayanan', $kd);
        return $this->db->delete($this->table);
	}

}
