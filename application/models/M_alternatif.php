<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_alternatif extends CI_Model
{

    public $table = 'alternatif';
    public $kd = 'kdAlternatif';
    public $order = 'DESC';

    // get all
    function get_all()
    {
		$this->db->join('obat','obat.kdObat=alternatif.kdObat','left');
		$this->db->join('layanan','layanan.kdLayanan=alternatif.kdLayanan','left');
        //$this->db->order_by($this->kd, $this->order);
        return $this->db->get($this->table)->result();
	}

	public function get_nilai($kdAlternatif)
	{
		$this->db->where('kdAlternatif', $kdAlternatif);
		$this->db->join('kriteria','kriteria.kdkriteria=nilai.kdkriteria','left');
		$this->db->join('subkriteria','nilai.kdSubKriteria=subkriteria.kdSubKriteria','left');
		return $this->db->get('nilai')->result();
	}

	public function get_nilai_value_by_kriteria_and_alternatif($kdAlternatif, $kdKriteria)
	{
		$this->db->where('nilai.kdAlternatif', $kdAlternatif);
		$this->db->where('nilai.kdKriteria', $kdKriteria);
		$this->db->join('subkriteria','nilai.kdSubKriteria=subkriteria.kdSubKriteria','left');
		return $this->db->get('nilai')->row()->value;
	}

	public function get_nilai_by_alternatif_and_kriteria($kdAlternatif, $kdKriteria)
	{
		$this->db->where('nilai.kdAlternatif', $kdAlternatif);
		$this->db->where('nilai.kdKriteria', $kdKriteria);
		$this->db->join('subkriteria','nilai.kdSubKriteria=subkriteria.kdSubKriteria','left');
		return $this->db->get('nilai')->row();
	}
	
    function get_where($where)
    {
        $this->db->where($where);
        $this->db->order_by($this->kd, $this->order);
        return $this->db->get($this->table);
    }

    // get data by kd
    function get_by_kd($kd)
    {
		$this->db->join('obat','obat.kdObat=alternatif.kdObat','left');
		$this->db->join('layanan','layanan.kdLayanan=alternatif.kdLayanan','left');
        $this->db->where($this->kd, $kd);
        return $this->db->get($this->table)->row();
    }

    // insert data
    function insert($data)
    {
        return $this->db->insert($this->table, $data);
	}
	
	public function insert_to_nilai($data)
	{
		return $this->db->insert('nilai', $data);
	}

    // update data
    function update($kd, $data)
    {
        $this->db->where('kdAlternatif', $kd);
        return $this->db->update($this->table, $data);
	}
	
	public function delete($kd)
	{
		$this->db->where('kdAlternatif', $kd);
        return $this->db->delete($this->table);
	}

	public function delete_nilai($kd)
	{
		$this->db->where('kdAlternatif', $kd);
        return $this->db->delete('nilai');
	}
	
}
