<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_admin extends CI_Model
{

    public $table = 'admin';
    public $kd = 'kdAdmin';
    public $order = 'DESC';

    // get all
    function get_all()
    {
        $this->db->order_by($this->kd, $this->order);
        return $this->db->get($this->table)->result();
	}
	
    function get_where($where)
    {
        $this->db->where($where);
        $this->db->order_by($this->kd, $this->order);
        return $this->db->get($this->table);
    }

    // get data by kd
    function get_by_kd($kd)
    {
        $this->db->where($this->kd, $kd);
        return $this->db->get($this->table)->row();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($kd, $data)
    {
        $this->db->where('kdAdmin', $kd);
        $this->db->update($this->table, $data);
    }

    function cek_login($where){   
        return $this->db->get_where($this->table,$where);
    }

    public function delete($kd)
    {
        $this->db->where('kdAdmin', $kd);
        return $this->db->delete($this->table);
    }

}
