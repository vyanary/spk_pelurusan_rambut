<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_kriteria extends CI_Model
{

    public $table = 'kriteria';
    public $kd = 'kdKriteria';
    public $order = 'DESC';

    // get all
    function get_all()
    {
        //$this->db->order_by($this->kd, $this->order);
        return $this->db->get($this->table)->result();
	}
	
    function get_where($where)
    {
        $this->db->where($where);
        $this->db->order_by($this->kd, $this->order);
        return $this->db->get($this->table);
    }

    // get data by kd
    function get_by_kd($kd)
    {
        $this->db->where($this->kd, $kd);
        return $this->db->get($this->table)->row();
    }

    // insert data
    function insert($data)
    {
        return $this->db->insert($this->table, $data);
	}
	
	function update_to_subkriteria($kd, $data)
    {
		$this->db->where('kdSubKriteria', $kd);
        return $this->db->update('subkriteria', $data);
    }

    // update data
    function update($kd, $data)
    {
        $this->db->where('kdKriteria', $kd);
        return $this->db->update($this->table, $data);
	}
	
	public function delete($kd)
	{
		$this->db->where('kdKriteria', $kd);
        return $this->db->delete($this->table);
	}

	public function delete_subkriteria($kd)
	{
		$this->db->where('kdKriteria', $kd);
        return $this->db->delete('subkriteria');
	}

    public function get_sub_kriteria($kdKriteria)
    {
        $this->db->where('kdKriteria', $kdKriteria);
        return $this->db->get('subkriteria')->result();
	}

	public function get_sub_kriteria_by_kd($kdSubKriteria)
    {
        $this->db->where('kdSubKriteria', $kdSubKriteria);
        return $this->db->get('subkriteria')->row();
	}
	
	public function get_min_max_nilai_alternatif($kdKriteria)
	{
		$kriteria = $this->get_by_kd($kdKriteria);
		if($kriteria->sifat=="B"){
			$this->db->where('nilai.kdKriteria', $kdKriteria);
			$this->db->join('subkriteria', 'subkriteria.kdSubKriteria=nilai.kdSubkriteria');
			$this->db->select_max('value');
			return $this->db->get('nilai')->row()->value;
		}elseif($kriteria->sifat=="C"){
			$this->db->where('nilai.kdKriteria', $kdKriteria);
			$this->db->join('subkriteria', 'subkriteria.kdSubKriteria=nilai.kdSubkriteria');
			$this->db->select_min('value');
			return $this->db->get('nilai')->row()->value;
		}
	}

	public function get_sum_nilai_alternatif_perkriteria($kdKriteria)
	{
		$this->db->where('nilai.kdKriteria', $kdKriteria);
		$this->db->join('subkriteria', 'subkriteria.kdSubKriteria=nilai.kdSubkriteria');
		$this->db->select_sum('value');
		return $this->db->get('nilai')->row()->value;
	}
}
